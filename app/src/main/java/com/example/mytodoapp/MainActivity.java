package com.example.mytodoapp;

import android.content.Intent;
import android.os.Bundle;

import com.example.mytodoapp.data.DatabaseHandler;
import com.example.mytodoapp.model.TodoItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private EditText todoItem;
    private Button saveBtn;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //init dbhandler oncreate
        databaseHandler = new DatabaseHandler(this);

        byPassActivity();
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
    }

    //bypass activity
    private void byPassActivity(){
        if(databaseHandler.getItemsCount() > 0){
            startActivity(new Intent(MainActivity.this, ListActivity.class));
            finish();
        }
    }
    private void saveItem(View view){
        //create object
        TodoItem item = new TodoItem();
        //get input
        String todo = todoItem.getText().toString().trim();
        //pass the value to the item object
        item.setTodoItem(todo);
        //save to db
        databaseHandler.addItem(item);
        Snackbar.make(view, "item saved", Snackbar.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        }, 1200);
    }

    private void showPopup(){
        builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.popup, null);
        todoItem = view.findViewById(R.id.popup_item);
        saveBtn = view.findViewById(R.id.popup_save_btn);

        //when save button is clicked
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check if fields are not empty
                if(!todoItem.getText().toString().isEmpty()){
                    saveItem(view);
                }else{
                    Snackbar.make(view, "Enter a todo item", Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        });

        //invoke the builder
        builder.setView(view);
        //create the dialog object
        dialog = builder.create();
        //show dialog
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}