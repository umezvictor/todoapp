package com.example.mytodoapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mytodoapp.data.DatabaseHandler;
import com.example.mytodoapp.model.TodoItem;
import com.example.mytodoapp.ui.RecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    //init fields
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private List<TodoItem> itemList;
    private DatabaseHandler dbHandler;

    //floating action button
    private FloatingActionButton fab;

    //enable popup
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    //input fields
    private EditText todoItem;
    private Button saveButton;

    private static final String TAG = "LIST ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //init recyclerview
        recyclerView = findViewById(R.id.list_activity_recyclerView);

        fab = findViewById(R.id.activity_list_fab);
        dbHandler = new DatabaseHandler(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //get items from db
        itemList = new ArrayList<>();
        itemList = dbHandler.getAllTodoItems();//returns a list of todoItems

        /*
        for(TodoItem item : itemList){
            Log.d(TAG, "OnCreate " + item.getTodoItem());
        }
        */
        recyclerViewAdapter = new RecyclerViewAdapter(this, itemList);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.notifyDataSetChanged();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
    }

    //display popup when fab is clicked
    private void showPopup(){
        //init popup
        builder = new AlertDialog.Builder(this);
        //get view
        View view = getLayoutInflater().inflate(R.layout.popup, null);
        //set input fields
        todoItem = view.findViewById(R.id.popup_item);
        saveButton = view.findViewById(R.id.popup_save_btn);
        //show dialog
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
        //set onClickListener
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!todoItem.getText().toString().isEmpty()){
                    //save
                    saveItem(view);
                }else{
                    Snackbar.make(view, "Todo item can't be empty", Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        });
        //save item
    }

    //save item function
    //copy the same function in main activity
    private void saveItem(View view){
        //create object
        TodoItem item = new TodoItem();
        //get input
        String todo = todoItem.getText().toString().trim();
        //pass the value to the item object
        item.setTodoItem(todo);
        //save to db
        dbHandler.addItem(item);
        Snackbar.make(view, "item saved", Snackbar.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alertDialog.dismiss();
                startActivity(new Intent(ListActivity.this, ListActivity.class));
                finish();
            }
        }, 1200);
    }
}