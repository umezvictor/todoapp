package com.example.mytodoapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.mytodoapp.model.TodoItem;
import com.example.mytodoapp.util.Constants;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private final Context context;

    public DatabaseHandler(@Nullable Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table here
        String CREATE_TODO_TABLE = "CREATE TABLE " + Constants.TABLE_NAME + "("
                + Constants.KEY_ID + " INTEGER PRIMARY KEY,"
                + Constants.KEY_TODO_ITEM + " TEXT,"
                + Constants.KEY_DATE_ADDED + " LONG);";
        db.execSQL(CREATE_TODO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);
        onCreate(db);
    }

    //CRUD OPERATIONS
    public void addItem(TodoItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.KEY_TODO_ITEM, item.getTodoItem());
        values.put(Constants.KEY_DATE_ADDED, java.lang.System.currentTimeMillis());
        db.insert(Constants.TABLE_NAME, null, values);
       // Log.d("DBHandler ", "Item added");
    }

    //get single item
    public TodoItem todoItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Constants.TABLE_NAME,
                new String[]{Constants.KEY_ID,
                        Constants.KEY_TODO_ITEM,
                        Constants.KEY_DATE_ADDED},
                Constants.KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        TodoItem todoItem = new TodoItem();
        if (cursor != null) {
            todoItem.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_ID))));
            todoItem.setTodoItem(cursor.getString(cursor.getColumnIndex(Constants.KEY_TODO_ITEM)));

            //date
            DateFormat dateFormat = DateFormat.getDateInstance();
            String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_DATE_ADDED)))
                    .getTime());

            todoItem.setDateCreated(formatedDate);
        }
        return todoItem;
    }

    //get all todoItems
    public List<TodoItem> getAllTodoItems(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<TodoItem> itemList = new ArrayList<>();
        Cursor cursor = db.query(Constants.TABLE_NAME,
                new String[]{Constants.KEY_ID,
                        Constants.KEY_TODO_ITEM,
                        Constants.KEY_DATE_ADDED},
                null, null, null, null,Constants.KEY_DATE_ADDED + " DESC");

        if(cursor.moveToFirst()){
            do{
                TodoItem todoItem = new TodoItem();
                todoItem.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_ID))));
                todoItem.setTodoItem(cursor.getString(cursor.getColumnIndex(Constants.KEY_TODO_ITEM)));
                //date
                DateFormat dateFormat = DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_DATE_ADDED)))
                        .getTime());

                todoItem.setDateCreated(formatedDate);
                itemList.add(todoItem);
            }while(cursor.moveToNext());

        }
        return itemList;
    }

    //update item
    public int updateItem (TodoItem item){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_TODO_ITEM, item.getTodoItem());
        values.put(Constants.KEY_DATE_ADDED, java.lang.System.currentTimeMillis());//timestamp
        //update row
        return db.update(Constants.TABLE_NAME, values, Constants.KEY_ID + "=?",
                new String[]{String.valueOf(item.getId())});
    }

    //delete item
    public void deleteItem(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.TABLE_NAME,
                Constants.KEY_ID + "=?",
                new String[]{String.valueOf(id)});
        //close
        db.close();
    }

    //get items count - total number of todo items
    public int getItemsCount(){
        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }
}
