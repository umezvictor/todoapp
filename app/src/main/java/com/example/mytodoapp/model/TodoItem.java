package com.example.mytodoapp.model;

public class TodoItem {

    private int id;
    private String todoItem;
    private String dateCreated;

    public TodoItem(int id, String todoItem, String dateCreated){
        this.id = id;
        this.todoItem = todoItem;
        this.dateCreated = dateCreated;
    }

    public TodoItem(String todoItem, String dateCreated){
        this.todoItem = todoItem;
        this.dateCreated = dateCreated;
    }
    //empty constructor
    public TodoItem(){}

//getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTodoItem() {
        return todoItem;
    }

    public void setTodoItem(String todoItem) {
        this.todoItem = todoItem;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }
}
