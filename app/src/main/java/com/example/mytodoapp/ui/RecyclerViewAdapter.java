package com.example.mytodoapp.ui;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytodoapp.R;
import com.example.mytodoapp.data.DatabaseHandler;
import com.example.mytodoapp.model.TodoItem;
import com.google.android.material.snackbar.Snackbar;

import java.text.MessageFormat;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<TodoItem> itemList;

    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private LayoutInflater inflater;

    public RecyclerViewAdapter(Context context, List<TodoItem> itemList){
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items_list_row, viewGroup, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, int position) {
        TodoItem item = itemList.get(position);
        viewHolder.todoItem.setText(MessageFormat.format("Item: {0}", item.getTodoItem()));
        viewHolder.dateAdded.setText(MessageFormat.format("Added on: {0}", item.getDateCreated()));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView todoItem;
        public TextView dateAdded;
        public Button editBtn;
        public Button deleteBtn;
        public int id;

        public ViewHolder(@NonNull View itemView, Context ctx) {
            super(itemView);

            context = ctx;

            todoItem = itemView.findViewById(R.id.item_list_row_item_name);
            dateAdded = itemView.findViewById(R.id.item_list_row_date_added);
            editBtn = itemView.findViewById(R.id.edit_btn);
            deleteBtn = itemView.findViewById(R.id.delete_btn);

            editBtn.setOnClickListener(this);
            deleteBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position;
            position = getAdapterPosition();
            TodoItem item = itemList.get(position);

            switch (view.getId()){
                case R.id.edit_btn:
                    //edit button
                    editItem(item);
                    break;
                case R.id.delete_btn:
                //delete btn
                    deleteItem(item.getId());
                    break;
            }
        }

        private void deleteItem(final int id){
            //prepare view
            builder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.confirmation_popup, null);
            //get buttons
            Button noButton = view.findViewById(R.id.no_btn);
            Button yesButton = view.findViewById(R.id.yes_btn);
            //show view
            builder.setView(view);
            dialog = builder.create();
            dialog.show();

            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseHandler db = new DatabaseHandler(context);
                    db.deleteItem(id);
                    itemList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    dialog.dismiss();
                }
            });

            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }

        private void editItem(final TodoItem newItem){
            builder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.popup, null);

            Button saveBtn;
            TextView title;
            final EditText todoItem;

            todoItem = view.findViewById(R.id.popup_item);
            saveBtn = view.findViewById(R.id.popup_save_btn);
            title = view.findViewById(R.id.popup_title);
            saveBtn.setText("Update");
            title.setText("Edit todo item");
            todoItem.setText(newItem.getTodoItem());
            builder.setView(view);
            dialog = builder.create();
            dialog.show();

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseHandler db = new DatabaseHandler(context);
                    newItem.setTodoItem(todoItem.getText().toString());

                    if(!todoItem.getText().toString().isEmpty()){
                        db.updateItem(newItem);
                        notifyItemChanged(getAdapterPosition(), newItem);
                    }else{
                        Snackbar.make(view, "Empty field not allowed",
                                Snackbar.LENGTH_SHORT )
                                .show();
                    }

                    dialog.dismiss();
                }
            });
        }
    }
}
