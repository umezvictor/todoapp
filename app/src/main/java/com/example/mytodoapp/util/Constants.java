package com.example.mytodoapp.util;

public class Constants {
    public static final int DB_VERSION = 2;
    public static final String DB_NAME = "todoDB";
    public static final String TABLE_NAME = "todoTable";

    //table fields
    public static final String KEY_ID = "id";
    public static final String KEY_TODO_ITEM = "item";
    public static final String KEY_DATE_ADDED = "date_created";
}
